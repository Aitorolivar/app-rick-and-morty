import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: 'home', loadChildren: () => import('./pages/home-page/home-page.module').then(m => m.HomePageModule)},
  {path: 'characters', loadChildren: () => import('./pages/character-page/character-page.module').then(m => m.CharacterPageModule)},
  {path: 'locations', loadChildren: () => import('./pages/location-page/location-page.module').then(m => m.LocationPageModule)},
  {path: 'episodes', loadChildren: () => import('./pages/episodes-page/episodes-page.module').then(m => m.EpisodesPageModule)},
  {path: 'about', loadChildren: () => import('./pages/about-page/about-page.module').then(m => m.AboutPageModule)},
  {path: '', redirectTo:'home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
