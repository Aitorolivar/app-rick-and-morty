import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CharacterPageRoutingModule } from './character-page-routing.module';
import { CharacterPageComponent } from './character-page.component';
import { CharacterGalleryComponent } from './character-gallery/character-gallery.component';
import { CharacterDetailsComponent } from './character-details/character-details.component';


@NgModule({
  declarations: [
    CharacterPageComponent,
    CharacterGalleryComponent,
    CharacterDetailsComponent
  ],
  imports: [
    CommonModule,
    CharacterPageRoutingModule
  ]
})
export class CharacterPageModule { }
