import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-character-gallery',
  templateUrl: './character-gallery.component.html',
  styleUrls: ['./character-gallery.component.scss']
})
export class CharacterGalleryComponent implements OnInit {
  @Input() list:any = [];

  constructor() { }

  ngOnInit(): void {
  }

}
