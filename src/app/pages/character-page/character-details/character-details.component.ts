import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CharactersService } from 'src/app/shared/services/characters.service';

@Component({
  selector: 'app-character-details',
  templateUrl: './character-details.component.html',
  styleUrls: ['./character-details.component.scss']
})
export class CharacterDetailsComponent implements OnInit {
  public personDetail:any = {};

  constructor(private route:ActivatedRoute, private characterservice: CharactersService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((param)=> {
      let charactersId = param.get('id');
      this.characterservice.getFoundId(charactersId).subscribe((details)=> {
        this.personDetail = details;
      })
    })
  }

}
