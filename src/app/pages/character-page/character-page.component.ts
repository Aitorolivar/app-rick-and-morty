import { Component, OnInit } from '@angular/core';
import { CharactersService } from 'src/app/shared/services/characters.service';

@Component({
  selector: 'app-character-page',
  templateUrl: './character-page.component.html',
  styleUrls: ['./character-page.component.scss']
})
export class CharacterPageComponent implements OnInit {
  public characterData:any = [];
  constructor(private charactersservice:CharactersService) { }

  ngOnInit(): void {
    this.charactersservice.getCharacters().subscribe((data)=> {
      this.characterData = data.results
    }) 
  }

}
