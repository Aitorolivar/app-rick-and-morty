import { Component, OnInit } from '@angular/core';
import { EpisodesService } from 'src/app/shared/services/episodes.service';

@Component({
  selector: 'app-episodes-page',
  templateUrl: './episodes-page.component.html',
  styleUrls: ['./episodes-page.component.scss']
})
export class EpisodesPageComponent implements OnInit {
  public episodesData:any =[];

  constructor(private episodesservice:EpisodesService) { }

  ngOnInit(): void {
    this.episodesservice.getEpisodes().subscribe((data)=> {
      this.episodesData = data.results;
    })
  }

}
