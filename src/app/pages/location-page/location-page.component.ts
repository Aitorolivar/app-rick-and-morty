import { Component, OnInit } from '@angular/core';
import { LocationsService } from 'src/app/shared/services/locations.service';

@Component({
  selector: 'app-location-page',
  templateUrl: './location-page.component.html',
  styleUrls: ['./location-page.component.scss']
})
export class LocationPageComponent implements OnInit {
  public locationData: any = [];

  constructor(private locationsservice:LocationsService) { }

  ngOnInit(): void {
    this.locationsservice.getLocations().subscribe((data)=>{
      this.locationData = data.results;
    })
  }

}
