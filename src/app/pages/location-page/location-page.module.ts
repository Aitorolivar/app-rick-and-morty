import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LocationPageRoutingModule } from './location-page-routing.module';
import { LocationPageComponent } from './location-page.component';
import { LocationGalleryComponent } from './location-gallery/location-gallery.component';


@NgModule({
  declarations: [
    LocationPageComponent,
    LocationGalleryComponent
  ],
  imports: [
    CommonModule,
    LocationPageRoutingModule
  ]
})
export class LocationPageModule { }
